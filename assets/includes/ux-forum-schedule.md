<!-- markdownlint-disable-next-line MD041 -->
#### Upcoming UX Forums

| Date       | Host                 | Speaker 1              | Speaker 2              | Speaker 3              |
|------------|----------------------|------------------------|------------------------|------------------------|
| 2025-01-22 | Andy Volpe           | Chad Lavimoniere       | Amelia Bauerly         |                        |
| 2025-02-05 | Chris Micek          | Taylor Vanderhelm      | Libor Vanc             |                        |
| 2025-02-19 | Paul Wright          |                        | Ilonah Pelaez          |                        |
| 2025-03-05 | Marcel van Remmerden | Jeremy Elder           | Pedro Moreira da Silva | Becka Lippert          |
| 2025-03-19 | Rayana Verissimo     | Austin Regnery         | Julia Miocene          |                        |
| 2025-04-02 | Taurie Davis         | Gina Doyle             | Nick Leonard           |                        |
| 2025-04-16 | Justin Mandell       | Camellia Yang          | Libor Vanc             | Sunjung Park           |
| 2025-04-30 | Emily Sybrant        | Alyssa Trinh           | Sascha Eggenberger     |                        |
| 2025-05-14 | Jacki Bouer          | Michael Fangman        | Tina Lise Ng           |                        |
| 2025-05-28 | APAC                 | Michael Le             | Alex Fracazo           | Bonnie Tsang           |
| 2025-06-11 | Andy Volpe           | Dan Mizzi-Harris       | Tim Noah               |                        |
| 2025-06-25 | Chris Micek          | Ilonah Pelaez          | Lina Fowler            |                        |
| 2025-07-09 | Justin Mandell       | Nick Brandt            | Veethika Mishra        |                        |
| 2025-07-23 | Marcel van Remmerden | Graham Bachelder       | Trevor Storey          | Annabel Gray           |
| 2025-08-06 | Jacki Bauer          | Nicolle Merrill        | Julia Miocene          |                        |
| 2025-08-20 | Taurie Davis         | Chad Lavimoniere       | Amelia Bauerly         |                        |
| 2025-09-03 | Paul Wright          | Taylor Vanderhelm      | Sunjung Park           |                        |
| 2025-09-17 | Emily Sybrant        |                        |                        |                        |
| 2025-10-01 | Rayana Verissimo     | Jeremy Elder           | Pedro Moreira da Silva | Becka Lippert          |
| 2025-10-15 | Andy Volpe           | Austin Regnery         | Emily Bauman           |                        |
| 2025-10-29 | Chris Micek          | Nick Leonard           | Divya Alagarsamy       |                        |
| 2025-11-12 | Justin Mandell       | Camellia Yang          | Libor Vanc             |                        |
| 2025-11-26 | Marcel van Remmerden | Alyssa Trinh           | Sascha Eggenberger     |                        |
| 2025-12-10 | Jacki Bauer          | Michael Fangman        | Tina Lise Ng           | Gina Doyle             |
| 2025-12-24 | APAC                 | Michael Le             | Alex Fracazo           | Bonnie Tsang           |
| 2026-01-07 | Taurie Davis         | Dan Mizzi-Harris       | Tim Noah               |                        |
| 2026-01-21 | Paul Wright          | Ilonah Pelaez          | Lina Fowler            |                        |
| 2026-02-04 | Emily Sybrant        | Nick Brandt            | Veethika Mishra        |                        |
| 2026-02-18 | Rayana Verissimo     | Graham Bachelder       | Trevor Storey          | Annabel Gray           |


Tip for Product Design Managers: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online [markdown generator](https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table).

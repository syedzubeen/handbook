---
title: "GitLab.tv All-Remote Channel Playbook"
description: All Remote Channel Playbook
---

{{% alert title="Note" color="warning" %}}
The GitLab.tv initiative is on-hold indefinitely, and not accepting submissions.
The content continues to be available publicly as a useful resource on remote work.
{{% /alert %}}

## Introduction

![GitLab collaboration illustration](/images/all-remote/gitlab-collaboration.jpg)

The [GitLab.tv All-Remote Channel](https://www.youtube.com/playlist?list=PLFGfElNsQthY6t1i0UXYr0wFW3DuvWww6) provides guidance and information to business owners, executives and managers on the ways in which they can transition to a remote workforce. Our audience of founders, CROs, CXOs, HR leaders, and managers work in all industries, particularly in tech and enterprise to midsize companies. For additional context, visit the GitLab [All-Remote Marketing Handbook](/handbook/ceo/office-of-the-ceo/workplace/).

The GitLab.tv All-Remote Channel can help enable viewers to create a remote strategy and establish basic remote management processes. They can also find engaging, actionable information that will help them identify the priorities necessary to successfully go remote and get back to a normal flow of business with a hybrid remote or fully remote team.

## GitLab.tv Playlist on YouTube

The GitLab.tv playlist is listed on GitLab's [official YouTube channel](https://www.youtube.com/playlist?list=PLFGfElNsQthY6t1i0UXYr0wFW3DuvWww6).

## Audience

![GitLab customer illustration](/images/all-remote/gitlab-customer-path.jpg)

The [GitLab.tv All-Remote Channel](https://www.youtube.com/playlist?list=PLFGfElNsQthY6t1i0UXYr0wFW3DuvWww6) informs business leaders and stakeholders about the various aspects of implementing and managing remote teams. For the purposes of this channel, we split the audience into two groups:

1. **People interested in remote work:** These viewers are looking for information on how to get started with remote work. This video content should aim to lower the barrier of entry to remote work by providing actionable guidelines and instructions. Case studies and anecdotal stories will also be of interest to these viewers. This audience is the most likely to binge on various types of content within the channel as they learn more about remote work.
1. **Experienced remote workers and leaders:** These viewers are looking for innovative tips and stories as well as case studies around remote work. The channel is more of an informative and idea-inspiring outlet for this audience.

## Related links and material

1. [GitLab All-Remote Marketing Handbook](/handbook/ceo/office-of-the-ceo/workplace/)
1. [GitLab Guide to All-Remote](/handbook/company/culture/all-remote/guide/)
1. [GitLab Remote Playbook](https://learn.gitlab.com/all-remote/remote-playbook)

---

Return to the main [Growth Marketing Handbook](/handbook/marketing/inbound-marketing/).

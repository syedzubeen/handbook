---
title: Solutions Architects People Leader Shadow Program
description: "A program enabling Solutions Architects (SA) to shadow a Solutions Architect people leader."
---

## Overview

The SA leadership shadow program is a 2-week immersion to experience the [skills required](/job-families/sales/solutions-architect/#manager-solutions-architects-responsibilities) to become an SA people leader structured around the four fundamental rules of Solutions Architecture leadership, referred to as the [3+1 Rules of Sales Engineering Leadership](https://wethesalesengineers.com/the-31-rules-of-sales-engineering-leadership/): Manage yourself, Develop and serve your people, Run your organization as a business, and Serve and delight your customers.

### Program Goals

- Experience firsthand how SA leaders embody and execute the 3+1 rules framework
- Understand leadership decision-making processes through direct observation
- Contribute to key initiatives while learning leadership principles
- Foster transparency, diversity, inclusion, and belonging
- Develop future SA leaders who can excel across all four rules

## Participating in the Program

### Eligibility

Candidates must:

1. Complete their [Individual Growth Plan (IGP)](/handbook/people-group/learning-and-development/career-development/igp-guide)
2. Review their IGP with their manager and discuss leadership aspirations
3. Review the [SA Manager Operating Rhythm](https://handbook.gitlab.com/handbook/solutions-architects/sa-manager/) and discuss this with your manager

### How to Apply

1. Create a merge request to add yourself to the [rotation schedule](#rotation-schedule)
2. Assign your manager and ask them to approve and merge the merge request
3. Coordinate shadowing arrangements through your manager

### Rotation Schedule

| Start Date | Shadow Name | Geo Preference (can be multiple) |
|-------|---------|---------|
| FY25 Q3 | [arun_kg](https://gitlab.com/arun_kg) | NEUR |
| FY26 Q1 | [Regnard Raquedan](https://gitlab.com/rraquedan) |  |
| FY26 Q1 | [Paul Dumaitre](https://gitlab.com/pdumaitre) |  |
| FY26 Q1 | [Nupur Sharma](https://gitlab.com/nsharma2) |  |
| FY26 Q2 | [Julia Gätjens](https://gitlab.com/jgaetjens) |  |
| FY26 Q3 |  |  |
| FY26 Q4 |  |  |
| FY27 Q1 |  |  |
| FY27 Q2 |  |  |

## Program Preparation

### Confidentiality

As a shadow, you'll have access to confidential information requiring compliance with the [Designated Insiders process](https://docs.google.com/document/d/1mcBtnfGbv4jSsJUklMQYyj2052MBHe4Lf9RkE-B9yvA/edit?tab=t.0#heading=h.b3pm8ljoip1y) for the entire quarter. Even after the quarter is complete, the program's success depends on continually maintaining this trust beyond the duration of the shadow.

### Getting Ready for Your Shadowing Experience

Ensure you've made necessary preparations before beginning.

Take time to review your calendar and confirm you can fully commit to this development opportunity. Verify that your current workload allows for dedicated shadowing time, and arrange coverage for any essential deals under your responsibility. Before starting, have a detailed conversation with your assigned manager to get clarity on the shadowing schedule and expectations.

To provide you with diverse leadership exposure, you'll be matched with a leader outside your direct reporting line. This intentional pairing allows you to observe and learn from different management approaches and leadership styles.

The SA leader you will shadow will maximise your exposure to a diverse set of activities, however some of them may not be available during your shadowing and this is up to the shadowing SA leader discretion.

### Program Focus

The shadow experience leading up to, during, and after the 2-week shadow is organized around the 3+1 rules framework:

#### Rule 0: Manage Yourself

- Develop [SMART goals](https://www.mindtools.com/pages/article/smart-goals.htm) for your shadow experience
- Schedule preparatory coffee chats with SA leaders
- Create a personal leadership development plan
- Identify key areas for growth and mentorship
- Build a 30-60-90 day transition plan for potential leadership roles

#### Rule 1: Develop and Serve Your People

- Participate in SA 1:1 meetings
- Join hiring manager/panel interviews
- Complete [Interview Training](https://university.gitlab.com/learn/course/interviewer-training)
- Practice giving feedback through [ride alongs](/handbook/solutions-architects/sa-practices/ride-alongs)
- Learn team development and retention strategies
- Get [TeamOps Certified](https://university.gitlab.com/learn/course/teamops/introduction-to-teamops/introduction?client=internal-team-members)

#### Rule 2: Run Your Organization as a Business

- Observe Geo SA deal review sessions
- Analyze regional performance data
- Contribute to QBR preparation and presentations
- Attend sales forecasting calls
- Study resource allocation and capacity planning
- Learn business metrics and KPIs

#### Rule 3: Serve and Delight Your Customers

- Shadow customer-facing leadership activities
- Learn escalation management processes
- Understand customer success metrics
- Observe strategic account planning
- Study customer satisfaction initiatives

### Skills Development

| Skill | Development Activity |
|-------|---------|
| Lead by Example | Maintain account responsibilities, demonstrate proactive customer engagement |
| Team Development | Complete the [TeamOps Certification](https://university.gitlab.com/learn/course/teamops/introduction-to-teamops/introduction?client=internal-team-members), participatae in team building |
| Talent Management | Complete [Interview Training](https://university.gitlab.com/learn/course/interviewer-training), and if possible, participate in the hiring process |
| Retention of Personnel | Take feedback from the Retrospective, prepare the QBR Retrospective Slide for your given team and lead the discussion on that slide, and build an initiative around improving one aspect of the team |
| Provide Feedback | Participate in customer calls and provide feedback to the SA |
| Communication | Participate in leadership meetings where appropriate, participate in note-taking |
| Sales Strategy | Engage in forecasting and strategy sessions |
| Mentorship | Mentor another SA |
| Personal Development | Identify key areas of growth and work with a mentor on growing areas in monthly cadenced meetings |

## Suggested Reading

- [Leaders Eat last](https://simonsinek.com/books/leaders-eat-last/)
- [Tribes](https://www.amazon.it/Tribes-We-Need-You-Lead/dp/1591842336)
- [Empathy: Why it matters](https://www.amazon.com/Empathy-Why-Matters-How-Get/dp/0399171401)
- [The Speed of Trust](https://speedoftrust.com/)
- [The Culture Map](https://www.amazon.co.uk/Culture-Map-Decoding-People-Cultures/dp/1610392760?pd_rd_w=P7BoC&content-id=amzn1.sym.ec8f623a-d4f7-4017-b387-58abf6ea18ca&pf_rd_p=ec8f623a-d4f7-4017-b387-58abf6ea18ca&pf_rd_r=59XH7ARR3R406KVPJ79G&pd_rd_wg=DCguM&pd_rd_r=753167c2-5900-4c19-b32f-537fbbf73058&pd_rd_i=1610392760)
- [The Chimp Paradox](https://www.amazon.com/Chimp-Paradox-Impulses-Determine-Happiness/dp/009193558X)
- [The Sales Engineer Manager's Handbook](https://www.amazon.co.uk/gp/product/B087GKLVBZ/ref=kinw_myk_ro_title)
- [Multipliers](https://www.amazon.co.uk/gp/product/B01KT18416/ref=kinw_myk_ro_title)
- [The Making of a Manager](https://www.amazon.co.uk/gp/product/B07DXGGDCQ/ref=kinw_myk_ro_title)
- [Team Topologies](https://www.amazon.co.uk/gp/product/B09JWT9S4D/ref=kinw_myk_ro_title)
- [How To Win Friends and Influence People](https://www.amazon.co.uk/gp/product/B07FY2WWZG/ref=kinw_myk_ro_title)
- [The Challenger Sale](https://www.amazon.com/Challenger-Sale-Control-Customer-Conversation/dp/1591844355/ref=sr_1_1?crid=1R47T9R3OXW2O&dib=eyJ2IjoiMSJ9.b2GpG6Bth1nvC0dnBNNhs-NlvKFJFO_IX9eN7bYqcw-2zWIHZveWBb_gdF_xxEY7g6FqlwMgljuwZMmkHjrKBb_c3lMVWuXoOTnM0539Loquzuny_t6zSUQ4fh-hv8LYEH2kPFQbEJvTyEe00SttSFaRw-YjznMCVkndai4sG1cqnAoBU8tJVKUpHMnl1eH4bl2YCcIg4jRmjjPMGzbf-8pxhJh1L4KJuVnnBhQxt-o.ON7BWwuTILo5LUwZ7F7hoUn_up45l5mrzS3UXCAnaAQ&dib_tag=se&keywords=the+challenger+sales&qid=1732094566&s=books&sprefix=the+challenger+sales%2Cstripbooks-intl-ship%2C198&sr=1-1)
- [The Qualified Sales Leader](https://www.amazon.co.uk/dp/B09236J2XX/ref=pe_33573471_635671331_TE_M1DP)

**Share Your Leadership Journey:** Have you read a book, article, or resource that significantly influenced your development as a leader? We welcome your contributions - please submit an MR to add your transformative reading recommendations to this page.

## Other Shadow Programs

- [CEO Shadow Program](/handbook/ceo/shadow/)
- [CFO Shadow Program](/handbook/finance/growth-and-development/cfo-shadow-program/)
